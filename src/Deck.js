import React, {Component} from 'react';
import { StyleSheet, Text, View, Animated, PanResponder, Dimensions } from 'react-native';

const SCREEN_WIDTH = Dimensions.get('window').width;

class Deck extends Component {

  constructor(props){
    super(props)
    const position = new Animated.ValueXY();
    // Handles gestures
    const panResponder = PanResponder.create({
    // 1). Called once a user taps down on screen (true -> take responsibility of all touches)
      onStartShouldSetPanResponder: () => true,
    // 2). Called once user drags/moves finger around screen
      onPanResponderMove: (event,gesture) => { // dx & dy total lifecycle of gesture
        position.setValue({x:gesture.dx,y:gesture.dy})
      },
    // 3). Called once user takes finger off screen
      onPanResponderRelease: () => {
        this.resetPosition();
      }
    });
    this.panResponder = panResponder;
    this.position = position
  }

  resetPosition() {
    Animated.spring(this.position,
      {toValue: {x:0, y: 0}
    }).start();
  }

  getCardStyle() { // Interpolation allows us to tie animations togther...

    const rotate = this.position.x.interpolate({ // Get current x position & interpolate
        inputRange: [-SCREEN_WIDTH * 1.5, 0, SCREEN_WIDTH * 1.5],
        outputRange: ['-120deg', '0deg', '120deg']
    });

    return {
      ...this.position.getLayout(),
      transform: [{ rotate }]
    }
  }

  renderCards() {
    return this.props.data.map((item, index) => {
      if(index === 0){
        return(
          <Animated.View
            key = {item.id}
            style={this.getCardStyle()}
            {...this.panResponder.panHandlers}>
            {this.props.renderCard(item)}
          </Animated.View>
        )
      }
      return this.props.renderCard(item);
    });
  }

  render() {
    return (
        <View>
          {this.renderCards()}
        </View>
    );
  }
}

const styles = StyleSheet.create({
  ball: {
    height:60,
    width:60,
    borderRadius:30,
    borderWidth:30,
    borderColor:'black'
  },
});

export default Deck;
